#version 410

struct VSInput {
    vec3  Position;                                             
    vec2  TexCoord;                                             
    vec3  Normal;    
    ivec4 BoneIDs;
    vec4  Weights;
};

//struct VSOutput{                                                                                    
//    vec2 TexCoord;                                                                 
//    vec3 Normal;                                                                   
//    vec3 WorldPos;                                                                 
//};

out vec2 TexCoord;                                                                 
out vec3 Normal;                                                                   
out vec3 WorldPos;  

in VSInput input;

const int MAX_BONES = 100;

uniform mat4 gWorld;
uniform mat4 gProj;
uniform mat4 gView;

uniform mat4 gBones[MAX_BONES];

void main()
{       
    mat4 BoneTransform = gBones[input.BoneIDs[0]] * input.Weights[0];
    BoneTransform     += gBones[input.BoneIDs[1]] * input.Weights[1];
    BoneTransform     += gBones[input.BoneIDs[2]] * input.Weights[2];
    BoneTransform     += gBones[input.BoneIDs[3]] * input.Weights[3];

    vec4 PosL      	= BoneTransform * vec4(input.Position, 1.0);
    gl_Position    	= gProj * gView * gWorld * PosL;
    TexCoord 		= input.TexCoord;
    vec4 NormalL   	= BoneTransform * vec4(input.Normal, 0.0);
    Normal  		= (gWorld * NormalL).xyz;
    WorldPos 		= (gWorld * PosL).xyz;                                
}