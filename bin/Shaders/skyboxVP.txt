#version 330

layout (location = 0) in vec4 vPosition;
out vec3 TexCoord;

uniform mat4 gWorld;
uniform mat4 gProj;
uniform mat4 gView;

void main(){
    gl_Position = gProj * gView * gWorld * vPosition;
    TexCoord    = vPosition.xyz;
}