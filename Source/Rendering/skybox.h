#ifndef SKYBOX_H
#define	SKYBOX_H

#include "Camera/camera.h"
#include "GLInterface/skybox_shader.h"
#include "Texture/cubemap_texture.h"
#include "Geometry/cube.h"

class SkyBox {

public:
    SkyBox();

    ~SkyBox();
    
    bool Init(const std::string& Directory,
              const std::string& PosXFilename,
              const std::string& NegXFilename,
              const std::string& PosYFilename,
              const std::string& NegYFilename,
              const std::string& PosZFilename,
              const std::string& NegZFilename,
			  Camera* p_cam);
    
    void Render();
    
private:    
    SkyboxShader* skyboxShader;
    CubemapTexture* skyboxTexture;
	Camera* pCamera;
	Cube geometry;
};

#endif	/* SKYBOX_H */