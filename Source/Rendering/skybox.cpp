#include "skybox.h"

SkyBox::SkyBox() {
    skyboxShader = NULL;
    skyboxTexture = NULL;
}


SkyBox::~SkyBox() {
	delete skyboxShader;
    delete skyboxTexture;
}


bool SkyBox::Init(const std::string& Directory,
                  const std::string& PosXFilename,
                  const std::string& NegXFilename,
                  const std::string& PosYFilename,
                  const std::string& NegYFilename,
                  const std::string& PosZFilename,
                  const std::string& NegZFilename,
				  Camera* p_cam)
{
	skyboxShader = new SkyboxShader();

    if (!skyboxShader->Init()) {
        printf("Error initializing the skybox technique\n");
        return false;
    }

    glUseProgram(skyboxShader->getProgram());
    skyboxShader->SetTextureUnit(0);
    
    skyboxTexture = new CubemapTexture("./Media/skybox/city/",
                                       PosXFilename,
                                       NegXFilename,
                                       PosYFilename,
                                       NegYFilename,
                                       PosZFilename,
                                       NegZFilename);

    if (!skyboxTexture->Load()) {
        return false;
    }
	geometry.generateObjectBuffer();
	pCamera = p_cam;
}


void SkyBox::Render()
{
    glUseProgram(skyboxShader->getProgram());
    
    GLint OldCullFaceMode;
    glGetIntegerv(GL_CULL_FACE_MODE, &OldCullFaceMode);
    GLint OldDepthFuncMode;
    glGetIntegerv(GL_DEPTH_FUNC, &OldDepthFuncMode);
    
    glCullFace(GL_FRONT);
    glDepthFunc(GL_LEQUAL);

	Matrix scale(1.0);
	scale.scale(100.0f);

	skyboxShader->SetWorldMatrix(scale.transpose());  
	skyboxShader->SetProjMatrix(pCamera->GetProjectionMatrix().transpose());  
	skyboxShader->SetViewMatrix(pCamera->GetViewMatrix().transpose());
   
	skyboxTexture->Bind(GL_TEXTURE0);
    
	geometry.draw();
    glCullFace(OldCullFaceMode);        
    glDepthFunc(OldDepthFuncMode);
}
