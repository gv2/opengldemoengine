#ifndef __INCLUDE_H__
#define __INCLUDE_H__

#define BUFFER_OFFSET(a) ((char*)NULL + (a))
#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }
#define INVALID_OGL_VALUE 0xFFFFFFFF
#define GLCheckError() (glGetError() == GL_NO_ERROR)

typedef unsigned int uint;

//System Headers
#include <iostream>

//FLTK & OpenGL Headers
#include <GL/glew.h>
#include <FL/glut.H>
#include <FL/gl.h>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>

#include "Math/math.h"
#include "Math/vector.h"
#include "Math/matrix.h"
#include "Math/quaternion.h"

using namespace Mathematics;

#endif //__INCLUDE_H__