/****************************************************************************************
*****************************************************************************************

  Creation Date: 01/06/2013

  Rowan Hughes - Basic Modern OpenGL Project Template (Mesh Loading & Skinning)
  Dependencies - FLTK (includes freeGLUT), GLEW, Assimp, Freeimage

*****************************************************************************************
****************************************************************************************/

//Some Windows Headers (For Time, IO, etc.)
#include <windows.h>
#include <mmsystem.h>

//Additional Headers
#include "include.h"
#include "Scene/Scene.h"

//Forward Declarations
void setupScene();
void updateScene();
void renderScene();
void exitScene();
void keypress(unsigned char key, int x, int y);
void mouseButton(int button, int state, int x, int y);
void mouseMovement(int x, int y);
void setViewport(int width, int height);

//Global Variables
int         windowId;           //Id for our window
DWORD		lastTickCount;      //Timekeeping
int         w_width=1280;       //Window Width
int         w_height=800;       //Window Height

//Camera Variables
float       mouseSensitivity = 0.1f;
bool        mouse_drag=false;

//Mouse Variables
GLfloat     mouseDeltaX = 0.0f;
GLfloat     mouseDeltaY = 0.0f;
GLfloat     lastX = 0.0f;
GLfloat     lastY = 0.0f;

std::shared_ptr<Scene> p_scene( new Scene() ); 

int main(int argc, char *argv[]) {      
	//FLTK Init Window
	Fl_Window win(w_width, w_height, "OpenGL Project Template");
	win.resizable(win);
	win.show(argc, argv);
	win.begin();
		// Set window position, size & create window
		glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
		glutInitWindowSize(w_width,w_height);
		glutInitWindowPosition(0,0);
		glutCreateWindow("OpenGL Project Template");
	win.end();   

	// Set GLUT callback functions 
	glutReshapeFunc(setViewport);
	glutDisplayFunc(renderScene);
	glutIdleFunc(updateScene);
	glutKeyboardFunc(keypress);
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouseMovement);

	//Initialize GLEW
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		//Problem: glewInit failed, something is seriously wrong.
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	//Setup OpenGL state & scene resources (models, etc)
	setupScene();
	//Show window & start update loop (Program stays in here until it ends)
	return(Fl::run());
}

void renderScene() {
	p_scene->Render();
}

void updateScene() {	
	p_scene->Update();

	// Draw the next frame
	glutPostRedisplay();
}

void setupScene() {
	std::cout<<"Initializing scene..."<<std::endl;
    
	// Initialise desired OpenGL state
	glClearColor( 114.0f/255.0f, 81.0f/255.0f, 129.0f/255.0f, 0.0 );

	glFrontFace(GL_CW);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	p_scene->Init(w_width, w_height);
}

void setViewport(int width, int height) {
	// Fill screen with viewport
	glViewport(0, 0, width, height);
	w_width = width;
	w_height = height;
	p_scene->UpdateViewport(width, height);
}

void keypress(unsigned char key, int x, int y) {
	// Test if user pressed ESCAPE (ascii 27), If so, exit the program
	if(key==27){
		exitScene();
	}
	if(key=='a'){
		p_scene->cam->Move(Vector3(-0.5,0,0));
	}
	if(key=='d'){
		p_scene->cam->Move(Vector3(0.5,0,0));
	}
	if(key=='w'){
		p_scene->cam->Move(Vector3(0,0,-0.5));
	}
	if(key=='s'){
		p_scene->cam->Move(Vector3(0,0,0.5));
	}
	if(key=='z'){
		p_scene->cam->Move(Vector3(0,-0.5,0));
	}
	if(key=='q'){
		p_scene->cam->Move(Vector3(0,0.5,0));
	}
	if(key=='x'){
		p_scene->cam->Reset();
	}
	// Other possible keypresses go here
	//if(key == 'a'){...}
}

void mouseButton(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			mouse_drag = true;  
			lastX = x;
			lastY = y;
		}
		else if(state == GLUT_UP) {
			mouse_drag = false;
		}
	}
}

void mouseMovement(int x, int y) {
	mouseDeltaX = x - lastX ;
	mouseDeltaY = y - lastY ;
	lastX = x;
	lastY = y;

	if(mouse_drag) {
		p_scene->cam->RotateByMouse(Vector2(mouseDeltaX*mouseSensitivity, mouseDeltaY*mouseSensitivity));
	}
}

void exitScene() {
	std::cout<<"Exiting scene..."<<std::endl;

	// Close window
	glutDestroyWindow(windowId);

	// Exit program
	exit(0);
}