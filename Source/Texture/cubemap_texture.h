#ifndef CUBEMAP_H
#define	CUBEMAP_H

#include <string>
#include "include.h"
#include <FreeImage.h>

using namespace std;

class CubemapTexture {

public:    
    CubemapTexture(const string& Directory,
                   const string& PosXFilename,
                   const string& NegXFilename,
                   const string& PosYFilename,
                   const string& NegYFilename,
                   const string& PosZFilename,
                   const string& NegZFilename);

    ~CubemapTexture();

    bool Load();

    void Bind(GLenum TextureUnit);

private:  
    string m_fileNames[6];
    GLuint m_textureObj;
	
    FREE_IMAGE_FORMAT m_pImage_format;
    FIBITMAP* m_pImage;
};

#endif	/* CUBEMAP_H */