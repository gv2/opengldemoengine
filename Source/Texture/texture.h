/****************************************************************************************
*****************************************************************************************

  Creation Date: 17/12/2012

  Rowan Hughes - Texture Container and Interface Class, Uses FreeImage Image Library

*****************************************************************************************
****************************************************************************************/

#ifndef __TEXTURE_H__
#define	__TEXTURE_H__

#include <string>

#include "include.h"
#include <FreeImage.h>

class Texture {

public:
	Texture(GLenum TextureTarget, const std::string& FileName);

    bool Load();

    void Bind(GLenum TextureUnit);

private:
    std::string m_fileName;
    GLenum m_textureTarget;
    GLuint m_textureObj;

    FREE_IMAGE_FORMAT m_pImage_format;
    FIBITMAP* m_pImage;
};

#endif