/****************************************************************************************
*****************************************************************************************

  Creation Date: 17/12/2012

  Rowan Hughes - Texture Container and Interface Class, Uses FreeImage Image Library

*****************************************************************************************
****************************************************************************************/

#include <iostream>
#include "texture.h"

Texture::Texture(GLenum TextureTarget, const std::string& FileName) {
    m_textureTarget = TextureTarget;
    m_fileName      = FileName;
    m_pImage       = NULL;
}

bool Texture::Load() {
	FREE_IMAGE_FORMAT m_pImage_format = FreeImage_GetFileType(m_fileName.c_str(),0);
    FIBITMAP* m_pImage = FreeImage_Load(m_pImage_format, m_fileName.c_str());

	m_pImage = FreeImage_ConvertTo32Bits(m_pImage);

    int w = FreeImage_GetWidth(m_pImage);
	int h = FreeImage_GetHeight(m_pImage);
    GLubyte* textura = new GLubyte[4*w*h];

    FreeImage_FlipVertical(m_pImage);

    char* pixeles = (char*)FreeImage_GetBits(m_pImage);
	  //FreeImage loads in BGR format, so you need to swap some bytes(Or use GL_BGR). 
	  for(int j= 0; j<w*h; j++) {
		  textura[j*4+0]= pixeles[j*4+2];
		  textura[j*4+1]= pixeles[j*4+1];
		  textura[j*4+2]= pixeles[j*4+0];
		  textura[j*4+3]= pixeles[j*4+3];
	  }

    glGenTextures(1, &m_textureObj);
    glBindTexture(m_textureTarget, m_textureObj);
    glTexImage2D(m_textureTarget, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)textura);
    glTexParameterf(m_textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(m_textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


    GLenum glErr = glGetError();
    if (glErr != GL_NO_ERROR) {
      printf("%s", gluErrorString(glErr));
      return false;
    }
    else
      return true;
}

void Texture::Bind(GLenum TextureUnit) {
    glActiveTexture(TextureUnit);
    glBindTexture(m_textureTarget, m_textureObj);
}