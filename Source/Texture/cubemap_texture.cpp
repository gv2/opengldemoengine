#include <iostream>
#include "cubemap_texture.h"

static const GLenum types[6] = {  GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                                  GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                                  GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                                  GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                                  GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                                  GL_TEXTURE_CUBE_MAP_NEGATIVE_Z };


CubemapTexture::CubemapTexture(const string& Directory,
                               const string& PosXFilename,
                               const string& NegXFilename,
                               const string& PosYFilename,
                               const string& NegYFilename,
                               const string& PosZFilename,
                               const string& NegZFilename)
{
    string::const_iterator it = Directory.end();
    it--;
    string BaseDir = (*it == '/') ? Directory : Directory + "/";
    
    m_fileNames[0] = BaseDir + PosXFilename;
    m_fileNames[1] = BaseDir + NegXFilename;
    m_fileNames[2] = BaseDir + PosYFilename;
    m_fileNames[3] = BaseDir + NegYFilename;
    m_fileNames[4] = BaseDir + PosZFilename;
    m_fileNames[5] = BaseDir + NegZFilename;
    
    m_textureObj = 0;
}

CubemapTexture::~CubemapTexture()
{
    if (m_textureObj != 0) {
        glDeleteTextures(1, &m_textureObj);
    }
}
    
bool CubemapTexture::Load()
{
    glGenTextures(1, &m_textureObj);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureObj);

	FREE_IMAGE_FORMAT m_pImage_format;
	FIBITMAP* m_pImage;

    for (unsigned int i = 0 ; i < ARRAY_SIZE_IN_ELEMENTS(types) ; i++) {
        m_pImage_format = FreeImage_GetFileType(m_fileNames[i].c_str(),0);
		m_pImage = FreeImage_Load(m_pImage_format, m_fileNames[i].c_str());

		m_pImage = FreeImage_ConvertTo32Bits(m_pImage);

		int w = FreeImage_GetWidth(m_pImage);
		int h = FreeImage_GetHeight(m_pImage);
		GLubyte* textura = new GLubyte[4*w*h];

		FreeImage_FlipVertical(m_pImage);

		char* pixeles = (char*)FreeImage_GetBits(m_pImage);
		  //FreeImage loads in BGR format, so you need to swap some bytes(Or use GL_BGR). 
		  for(int j= 0; j<w*h; j++) {
			  textura[j*4+0]= pixeles[j*4+2];
			  textura[j*4+1]= pixeles[j*4+1];
			  textura[j*4+2]= pixeles[j*4+0];
			  textura[j*4+3]= pixeles[j*4+3];
		  }

        glTexImage2D(types[i], 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)textura);
    }    
    
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);           
    
    GLenum glErr = glGetError();
    if (glErr != GL_NO_ERROR) {
      printf("%s", gluErrorString(glErr));
      return false;
    }
    else
      return true;
}
    
void CubemapTexture::Bind(GLenum TextureUnit)
{
    glActiveTexture(TextureUnit);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureObj);
}
