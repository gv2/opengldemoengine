#include "Camera.h"

//Constructor
Camera::Camera(const float _fov, const Vector2 _view_size, const Vector2 _near_far) {
    position = Vector3(0.0f, 3.0f, -15.0f);
    orientation = Quaternion(Vector3(0.0f, 0.0f, 0.0f));
    view_matrix = Matrix(1.0f);

    fov = _fov;
    view_size = _view_size;
    near_far = _near_far;
}

//Destructor
Camera::~Camera(void){}

//Reset
void Camera::Reset() {
    position = Vector3(0.0f, 0.0f, 5.0f);
    orientation = Quaternion(Vector3(0.0f, 0.0f, 0.0f));
    Update();
}

//Update Functions
void Camera::Update(void) {
    ComputeViewMatrix();
}

void Camera::ComputeViewMatrix(void) {
	Matrix rotation_matrix = (-orientation).matrix();   

    Matrix translation_matrix(1.0f);
	translation_matrix.translate(-position);
    
    view_matrix = rotation_matrix * translation_matrix;   
	view_matrix.inverse(inverse_view_matrix);
}

void Camera::ComputeProjectionMatrix(void) {
	projection_matrix.perspective(fov, view_size.x / float(view_size.y), near_far.x, near_far.y);
	projection_matrix.inverse(inverse_projection_matrix);
}

void Camera::ComputeOrthographicMatrix(int w, int h) {
  float right = w;
  float bottom = h;
  float left = 0.0f;
  float top = 0.0f;
  float _far = 1.0f;
  float _near = -1.0f;

  orthographic_matrix.m11 =  2/(right - left);
  orthographic_matrix.m21 =  0;                                                     
  orthographic_matrix.m31 =  0;                                                   
  orthographic_matrix.m41 =  0;                                                    

  orthographic_matrix.m12 =  0;                                                   
  orthographic_matrix.m22 =  2/(top - bottom);                                   
  orthographic_matrix.m32 =  0;                                                 
  orthographic_matrix.m42 =  0;                                                   

  orthographic_matrix.m13 =  0;                                                  
  orthographic_matrix.m23 =  0;                                                  
  orthographic_matrix.m33 =  2/(_far - _near);                                  
  orthographic_matrix.m43 =  0;                                         

  orthographic_matrix.m14 =  -(right + left) / (right - left);              
  orthographic_matrix.m24 =  -(top + bottom) / (top - bottom);              
  orthographic_matrix.m34 =  -(_far + _near) / (_far - _near);             
  orthographic_matrix.m44 =  1;                                    
}

void Camera::Move(Vector3 movement) {
    position += Vector3(view_matrix.m11, view_matrix.m12, view_matrix.m13) * movement.x;
    position += Vector3(view_matrix.m21, view_matrix.m22, view_matrix.m23) * movement.y;
    position -= Vector3(view_matrix.m31, view_matrix.m32, view_matrix.m33) * movement.z;
}

void Camera::Rotate(Vector3 angles) {
    Quaternion quat_x = Quaternion(angles.x * (180.0f / float(pi)), Vector3(1.0f, 0.0f, 0.0f));
    Quaternion quat_y = Quaternion(angles.y * (180.0f / float(pi)), Vector3(0.0f, 1.0f, 0.0f));
    orientation = quat_x * orientation * quat_y;
	orientation.normalize();
}

void Camera::RotateByMouse(Vector2 mouse_deltas) {
    Rotate(Vector3((float)(-mouse_deltas.y) / 1000.0f, (float)(-mouse_deltas.x) / 1000.0f, 0.0f));
}

//Setters
void Camera::SetPosition(Vector3 _position) {
    position = _position;
}

void Camera::SetOrientation(Quaternion _orientation) {
    orientation = _orientation;
}

void Camera::SetViewSize(Vector2 _view_size) {
    view_size = _view_size;
}

void Camera::SetNearFar(Vector2 _near_far) {
    near_far = _near_far;
}

void Camera::SetFOV(float _fov) {
    fov = _fov;
}

//Getters
Vector3 Camera::GetPosition(void) {
    return position;
}

Quaternion Camera::GetOrientation(void) {
    return orientation;
}

Vector2 Camera::GetViewSize(void) {
    return view_size;
}

Vector2 Camera::GetNearFar(void) {
    return near_far;
}

float Camera::GetFOV(void) {
    return fov;
}

Matrix Camera::GetViewMatrix(void) {
	return view_matrix;
}

Matrix Camera::GetInverseViewMatrix(void) {
    return inverse_view_matrix;
}

Matrix Camera::GetProjectionMatrix(void) {
    return projection_matrix;
}

Matrix Camera::GetInverseProjectionMatrix(void) {
    return inverse_projection_matrix;
}

Matrix Camera::GetOrthographicMatrix() {
  return orthographic_matrix;
}