#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "include.h"

class Camera {

public:
	//Constructor
	Camera(float _fov, Vector2 _view_size, Vector2 _near_far);
	//Destructor
	~Camera(void);

	//Update Functions
	void Update(void);
	void ComputeViewMatrix(void);
	void ComputeProjectionMatrix(void);
	void ComputeOrthographicMatrix(int width, int height);
	void Move(Vector3 movement);
	void Rotate(Vector3 angles);
	void RotateByMouse(Vector2 mouse_deltas);
	void Reset();

	//Setters
	void SetPosition(Vector3 _position);
	void SetOrientation(Quaternion _orientation);
	void SetViewSize(Vector2 _view_size);
	void SetNearFar(Vector2 _near_far);
	void SetFOV(float _fov);

	//Getters
	Vector3 GetPosition(void);
	Quaternion GetOrientation(void);
	Vector2 GetViewSize(void);
	Vector2 GetNearFar(void);
	float GetFOV(void);
	Matrix GetViewMatrix(void);
	Matrix GetInverseViewMatrix(void);
	Matrix GetProjectionMatrix(void);
	Matrix GetInverseProjectionMatrix(void);
	Matrix GetOrthographicMatrix();

private:
	// View Matrix
	Vector3 position;
	Quaternion orientation;
	Matrix view_matrix;
	Matrix inverse_view_matrix;
	
	// Projection Matrix
	Vector2 view_size;
	Vector2 near_far;
	float fov;
	Matrix projection_matrix;
	Matrix inverse_projection_matrix;
	Matrix orthographic_matrix;
};

#endif // __CAMERA_H__
