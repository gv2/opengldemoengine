#ifndef __GRID_H__
#define __GRID_H__

#include "include.h"
#include "shape.h"

class Grid {

public:
	Grid(int x_size, int z_size, float y_height, float spacing){
		for(int x = -x_size; x <= x_size; x++){
			vertex_positions.push_back(Vector4( x*spacing,  y_height,  -x_size*spacing,  1.0 ));
			vertex_positions.push_back(Vector4( x*spacing,  y_height,   x_size*spacing,  1.0 ));
		}

		for(int z = -z_size; z <= z_size; z++){
			vertex_positions.push_back(Vector4( -z_size*spacing, y_height,  z*spacing,  1.0 ));
			vertex_positions.push_back(Vector4(  z_size*spacing, y_height,  z*spacing,  1.0 ));
		}

		//Vector4 color = Vector4(184.0f/255.0f, 151.0f/255.0f, 199.0f/255.0f, 1.0f);
		Vector4 color = Vector4(1.0f, 1.0f, 1.0f, 1.0f);

		for (int i = 0; i < vertex_positions.size(); i++)
			vertex_colors.push_back(color);
	};

	void generateObjectBuffer() {
		//Initialize VAO
		glGenVertexArrays( 1, &vertex_array_object_id );
		glBindVertexArray( vertex_array_object_id );

		//Calc Array Sizes
		vertexArraySize = vertex_positions.size()*sizeof(Vector4);
		colorArraySize = vertex_colors.size()*sizeof(Vector4);

		//Initialize VBO
		glGenBuffers( 1, &buffer_object_id );
		glBindBuffer( GL_ARRAY_BUFFER, buffer_object_id );
		glBufferData( GL_ARRAY_BUFFER, vertexArraySize + colorArraySize, NULL, GL_STATIC_DRAW );
		glBufferSubData( GL_ARRAY_BUFFER, 0, vertexArraySize, (const GLvoid*)(&vertex_positions[0]) );
		glBufferSubData( GL_ARRAY_BUFFER, vertexArraySize, colorArraySize, (const GLvoid*)(&vertex_colors[0]));

		//Set up Vertex Arrays  
		glEnableVertexAttribArray( 0 ); //SimpleShader attrib at position 0 = "vPosition"
		glVertexAttribPointer( (GLuint)0, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
		glEnableVertexAttribArray( 1 ); //SimpleShader attrib at position 1 = "vColor"
		glVertexAttribPointer( (GLuint)1, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertexArraySize));

		//Unbind
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	void draw(){
		glBindVertexArray(vertex_array_object_id);

			glLineWidth(1);
			glEnable (GL_LINE_SMOOTH);
			glEnable (GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
				glDrawArrays( GL_LINES, 0, vertex_positions.size() );
			glDisable(GL_LINE_SMOOTH);
			glDisable (GL_BLEND);

		glBindVertexArray(0);
	}

protected:
	std::vector<Vector4> vertex_positions;
	std::vector<Vector4> vertex_colors;

	uint vertexArraySize;
	uint colorArraySize;

	uint buffer_object_id;
	uint vertex_array_object_id;
};

#endif // __GRID_H__