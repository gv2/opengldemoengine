#ifndef __RECT_H__
#define __RECT_H__

#include "include.h"
#include "shape.h"

class Rect : public Shape {

public:
	Rect(float x, float y, float z) {
		vertex_positions.push_back(Vector4( -x, -y,  z, 1.0 ));
		vertex_positions.push_back(Vector4( -x,  y,  z, 1.0 ));
		vertex_positions.push_back(Vector4(  x,  y,  z, 1.0 ));
		vertex_positions.push_back(Vector4(  x, -y,  z, 1.0 ));
		vertex_positions.push_back(Vector4( -x, -y, -z, 1.0 ));
		vertex_positions.push_back(Vector4( -x,  y, -z, 1.0 ));
		vertex_positions.push_back(Vector4(  x,  y, -z, 1.0 ));
		vertex_positions.push_back(Vector4(  x, -y, -z, 1.0 ));

		vertex_colors.push_back(Vector4( 0.0, 0.0, 0.0, 1.0 ));  // Black
		vertex_colors.push_back(Vector4( 1.0, 0.0, 0.0, 1.0 ));  // Red
		vertex_colors.push_back(Vector4( 1.0, 1.0, 0.0, 1.0 ));  // Yellow
		vertex_colors.push_back(Vector4( 0.0, 1.0, 0.0, 1.0 ));  // Green
		vertex_colors.push_back(Vector4( 0.0, 0.0, 1.0, 1.0 ));  // Blue
		vertex_colors.push_back(Vector4( 1.0, 0.0, 1.0, 1.0 ));  // Magenta
		vertex_colors.push_back(Vector4( 1.0, 1.0, 1.0, 1.0 ));  // White
		vertex_colors.push_back(Vector4( 0.0, 1.0, 1.0, 1.0 ));  // Cyan

		initRectangle();  
	};

private:
	void initRectangle() {
		quad( 1, 0, 3, 2, true);
		quad( 2, 3, 7, 6, true );
		quad( 3, 0, 4, 7, true);
		quad( 6, 5, 1, 2, true );
		quad( 4, 5, 6, 7, true );
		quad( 5, 4, 0, 1, true );
	}
};

#endif // __RECTANGLE_H__