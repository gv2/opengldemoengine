/****************************************************************************************
*****************************************************************************************

  Creation Date: 17/12/2012

  Rowan Hughes - Mesh Container and Interface Class, Uses Assimp Library

*****************************************************************************************
****************************************************************************************/

#ifndef __MESH_H__
#define	__MESH_H__

#include <map>
#include <vector>
#include <assert.h>
#include "include.h"
#include <assimp/Importer.hpp>     
#include <assimp/scene.h>           
#include <assimp/postprocess.h>    
#include "Texture/texture.h"

using namespace std;

class Mesh {
public:
	Mesh();

	~Mesh();

	bool LoadMesh(const string& Filename, const string& TexName = "./Media/red.png");

	void Render();
	
	uint NumBones() const { return m_NumBones; }
    
	void BoneTransform(float TimeInSeconds, vector<Matrix>& Transforms);

	Matrix* GetModelMatrix(){return &m_modelMatrix;};
	Matrix GetBoneWorldPosition(int i);
	void SetModelMatrix(Matrix& m);
	float getAnimDuration(){return (float)m_pScene->mAnimations[0]->mDuration;};
	float getTickTime(){return (float)(m_pScene->mAnimations[0]->mTicksPerSecond != 0 ? m_pScene->mAnimations[0]->mTicksPerSecond : 25.0f);};
	
private:
	#define NUM_BONES_PER_VERTEX 4
	
	struct BoneInfo {
		Matrix BoneOffset;
		Matrix WorldSpaceTransformation;
		Matrix FinalTransformation;        

		BoneInfo() {
			ZERO_MEM(&BoneOffset);
			ZERO_MEM(&FinalTransformation);
		}
	};
    
	struct VertexBoneData {        
		uint IDs[NUM_BONES_PER_VERTEX];
		float Weights[NUM_BONES_PER_VERTEX];

		VertexBoneData(){
			Reset();
		};
        
		void Reset() {
			ZERO_MEM(IDs);
			ZERO_MEM(Weights);        
		}
        
		void AddBoneData(uint BoneID, float Weight);
	};

	void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);    
	uint FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
	uint FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	uint FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
	const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const string NodeName);
	void ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const Matrix& ParentTransform);
	bool InitFromScene(const aiScene* pScene, const string& Filename, const string& TexName);
	void InitMesh(uint MeshIndex,
				const aiMesh* paiMesh,
				vector<Vector3>& Positions,
				vector<Vector3>& Normals,
				vector<Vector2>& TexCoords,
				vector<VertexBoneData>& Bones,
				vector<unsigned int>& Indices);
	void LoadBones(uint MeshIndex, const aiMesh* paiMesh, vector<VertexBoneData>& Bones);
	bool InitMaterials(const aiScene* pScene, const string& Filename, const string& TexName);
	void Clear();

	#define INVALID_MATERIAL 0xFFFFFFFF
  
	enum VB_TYPES {
		INDEX_BUFFER,
		POS_VB,
		NORMAL_VB,
		TEXCOORD_VB,
		BONE_VB,
		NUM_VBs            
	};

	GLuint m_VAO;
	GLuint m_Buffers[NUM_VBs];

	struct MeshEntry {
		MeshEntry() {
			NumIndices    = 0;
			BaseVertex    = 0;
			BaseIndex     = 0;
			MaterialIndex = INVALID_MATERIAL;
		}     
		unsigned int NumIndices;
		unsigned int BaseVertex;
		unsigned int BaseIndex;
		unsigned int MaterialIndex;
	};
    
	vector<MeshEntry> m_Entries;
	vector<Texture*> m_Textures;
     
	map<string,uint> m_BoneMapping; // maps a bone name to its index
	uint m_NumBones;
	vector<BoneInfo> m_BoneInfo;
	Matrix m_GlobalInverseTransform;
    
	Matrix m_modelMatrix;  //Initial Matrix applied to Models to put them at origin and at right scale
	Assimp::Importer m_Importer;
	const aiScene* m_pScene;
};

#endif