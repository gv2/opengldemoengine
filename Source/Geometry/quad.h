#ifndef __QUAD_H
#define __QUAD_H

#include "include.h"
#include "shape.h"
#include "GLInterface/fullscreen_quad.h"

class Quad {

public:
	Quad();
	void init();
	void draw(uint fb_tex);

private:
	std::vector<Vector2> points;
	std::vector<Vector2> texCoords;

	uint vertexArraySize;
	uint texArraySize;

	uint buffer_object_id;
	uint vertex_array_object_id;

	QuadShader* fullscreenQuadShader;

	void generateObjectBuffer();
};

#endif // __CUBE_H__