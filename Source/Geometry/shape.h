/**************************************************************************************************
***************************************************************************************************

  Creation Date: 20/10/2012

  Rowan Hughes - Simple Shape Super-Class From Which Other Shapes, i.e Cube and Quad, are Derived

***************************************************************************************************
**************************************************************************************************/

#ifndef __SHAPE_H__
#define __SHAPE_H__

#include <vector>
#include "include.h"

class Shape {

public:
	Shape() {
	};

	void generateObjectBuffer() {
		//Initialize VAO
		glGenVertexArrays( 1, &vertex_array_object_id );
		glBindVertexArray( vertex_array_object_id );

		//Calc Array Sizes
		vertexArraySize = points.size()*sizeof(Vector4);
		colorArraySize = colors.size()*sizeof(Vector4);

		//Initialize VBO
		glGenBuffers( 1, &buffer_object_id );
		glBindBuffer( GL_ARRAY_BUFFER, buffer_object_id );
		glBufferData( GL_ARRAY_BUFFER, vertexArraySize + colorArraySize, NULL, GL_STATIC_DRAW );
		glBufferSubData( GL_ARRAY_BUFFER, 0, vertexArraySize, (const GLvoid*)(&points[0]) );
		glBufferSubData( GL_ARRAY_BUFFER, vertexArraySize, colorArraySize, (const GLvoid*)(&colors[0]));

		//Set up Vertex Arrays  
		glEnableVertexAttribArray( 0 ); //SimpleShader attrib at position 0 = "vPosition"
		glVertexAttribPointer( (GLuint)0, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
		glEnableVertexAttribArray( 1 ); //SimpleShader attrib at position 1 = "vColor"
		glVertexAttribPointer( (GLuint)1, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertexArraySize));

		//Unbind
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	void draw(){
		glBindVertexArray(vertex_array_object_id);
			glEnable (GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
				glDrawArrays( GL_TRIANGLES, 0, points.size() );
			glDisable(GL_BLEND);
		glBindVertexArray(0);
	}

	Vector3 getPoint(int i){
		if(i < points.size())
			return Vector3(points[i].x, points[i].y, points[i].z);
	}

protected:
	std::vector<Vector4> points;
	std::vector<Vector4> colors;
	std::vector<Vector4> vertex_positions;
	std::vector<Vector4> vertex_colors;

	uint vertexArraySize;
	uint colorArraySize;

	uint buffer_object_id;
	uint vertex_array_object_id;

  void quad( int a, int b, int c, int d, bool color ){
	points.push_back(vertex_positions[a]);
	points.push_back(vertex_positions[b]);
	points.push_back(vertex_positions[c]);
	points.push_back(vertex_positions[a]);
	points.push_back(vertex_positions[c]);
	points.push_back(vertex_positions[d]);

	if(color) {
		colors.push_back(vertex_colors[a]); 
		colors.push_back(vertex_colors[b]); 
		colors.push_back(vertex_colors[c]); 
		colors.push_back(vertex_colors[a]); 
		colors.push_back(vertex_colors[c]); 
		colors.push_back(vertex_colors[d]);
	}
	else {
		colors.push_back(Vector4(1.0f,0.0f,0.0f,1.0f));
	}
  }
};

#endif // __SHAPE_H__