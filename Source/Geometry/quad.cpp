#include "quad.h"

Quad::Quad() {
	float size = 1.0f;

	// x,y vertex positions
	points.push_back(Vector2(-1.0,-1.0));
	points.push_back(Vector2( 1.0,-1.0)); 
	points.push_back(Vector2( 1.0, 1.0)); 
	points.push_back(Vector2( 1.0, 1.0));
	points.push_back(Vector2(-1.0, 1.0));
	points.push_back(Vector2(-1.0,-1.0));

	// per-vertex texture coordinates
	texCoords.push_back(Vector2(0.0,0.0));
	texCoords.push_back(Vector2(1.0,0.0));
	texCoords.push_back(Vector2(1.0,1.0));
	texCoords.push_back(Vector2(1.0,1.0));
	texCoords.push_back(Vector2(0.0,1.0));
	texCoords.push_back(Vector2(0.0,0.0));
}

void Quad::init() {	
	fullscreenQuadShader = new QuadShader();
	fullscreenQuadShader->Init();
	this->generateObjectBuffer();
}

void Quad::generateObjectBuffer() {
	//Initialize VAO
	glGenVertexArrays( 1, &vertex_array_object_id );
	glBindVertexArray( vertex_array_object_id );

	//Calc Array Sizes
	vertexArraySize = points.size()*sizeof(Vector2);
	texArraySize = texCoords.size()*sizeof(Vector2);

	//Initialize VBO
	glGenBuffers( 1, &buffer_object_id );
	glBindBuffer( GL_ARRAY_BUFFER, buffer_object_id );
	glBufferData( GL_ARRAY_BUFFER, vertexArraySize + texArraySize, NULL, GL_STATIC_DRAW );
	glBufferSubData( GL_ARRAY_BUFFER, 0, vertexArraySize, (const GLvoid*)(&points[0]) );
	glBufferSubData( GL_ARRAY_BUFFER, vertexArraySize, texArraySize, (const GLvoid*)(&texCoords[0]));

	//Set up Vertex Arrays  
	glEnableVertexAttribArray( 0 ); //quadShader attrib at position 0 = "vPosition"
	glVertexAttribPointer( (GLuint)0, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	glEnableVertexAttribArray( 1 ); //quadShader attrib at position 1 = "TexCoord"
	glVertexAttribPointer( (GLuint)1, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertexArraySize));

	//Unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Quad::draw(uint fb_tex){
	glUseProgram(fullscreenQuadShader->getProgram());
	glBindVertexArray(vertex_array_object_id);
			glActiveTexture (GL_TEXTURE0);
			glBindTexture (GL_TEXTURE_2D, fb_tex);
				glDrawArrays( GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
	glUseProgram(0);
}