#include "scene.h"

Scene::Scene() : grid(100,100,-1, 4){
	currentTime = 0.0f;
	t = 0.0f;
	accumulator = 0.0f;
}

Scene::~Scene() {
	delete cam;
	delete simpleShader;
	delete skinningShader;
}

void Scene::Init(int w_width, int w_height) {
	//Initialize Camera
	worldMat.identity();

	cam = new Camera(45.0f, Vector2(w_width, w_height), Vector2(0.001f, 400.0f));
	cam->ComputeProjectionMatrix();
	cam->ComputeOrthographicMatrix(w_width, w_height);
	cam->Update();

	//Initialize Skybox
	skybox.Init(".",
		"right.jpg",
		"left.jpg",
		"top.jpg",
		"bot.jpg",
		"front.jpg",
		"back.jpg",
		cam);

	//Initialize Shader
	simpleShader = new Shader("./Shaders/simpleVertexShader.txt", "./Shaders/simpleFragmentShader.txt");
	depthShader = new DepthShader();
	skinningShader = new SkinningShader();
	if (!skinningShader->Init()) {
		printf("Error initializing the lighting technique\n");
	}
	depthShader->Init();

	//Initialize Object Mesh Buffers
	grid.generateObjectBuffer();
	cube.generateObjectBuffer();
	quad.init();

	glUseProgram(skinningShader->getProgram());

	directionalLight.Color = Vector3(1.0f, 1.0f, 1.0f);
	directionalLight.AmbientIntensity = 0.55f;
	directionalLight.DiffuseIntensity = 0.9f;
	directionalLight.Direction = Vector3(1.0f, 0.0, 0.0);

	skinningShader->SetColorTextureUnit(0);
	skinningShader->SetDirectionalLight(directionalLight);
	skinningShader->SetMatSpecularIntensity(0.0f);
	skinningShader->SetMatSpecularPower(0);

	//Load Meshes
	if (!BVHModel.LoadMesh("./Media/Jump001.bvh", "./Media/red.png")) {
		printf("Mesh load failed\n");          
	}
	if (!md5Model.LoadMesh("./Media/models/boblampclean.md5mesh")) {
	  printf("Mesh load failed\n");          
	}

	//A transformation sequence for Guard Model.
	Matrix transformationMatrix = translate(worldMat, Vector3(5.0f,-1.0f,0.0f));
	transformationMatrix = rotate(transformationMatrix, 90.0f, Vector3(1.0f, 0.0f, 0.0f));
	transformationMatrix = rotate(transformationMatrix, 180.0f, Vector3(0.0f, 0.0f, 1.0f));
	transformationMatrix = scale(transformationMatrix, 0.11f);
	md5Model.SetModelMatrix(transformationMatrix);

	//A transformation sequence for the Jumper Model.
	transformationMatrix = translate(worldMat, Vector3(-5.0f,-0.75f,4.0f));
	transformationMatrix = rotate(transformationMatrix, 0.0f, Vector3(0.0f, 1.0f, 0.0f));
	transformationMatrix = scale(transformationMatrix, 4.0f);
	BVHModel.SetModelMatrix(transformationMatrix);

	//Set up Render Target
	rendertarget.init(w_width, w_height);
	depthtarget.init(w_width, w_height);

	m_startTime = GetCurrentTimeMillis();
}

void Scene::Update() {
	// Update The Camera
	cam->Update();

	float newTime = time();
    float deltaTime = newTime - currentTime;

	currentTime = newTime;

	// update discrete time
	accumulator += deltaTime;

	while (accumulator>=dt) {
		accumulator -= dt;
		t += dt;
	}
}

void Scene::Render() {
	rendertarget.bind();
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor( 114.0f/255.0f, 81.0f/255.0f, 129.0f/255.0f, 0.0 );

	skybox.Render();
	
	glUseProgram(simpleShader->getProgram());
	//N.B. Remember that for OpenGL we must transpose the matrices, different co-ordinate system (OR GL_FALSE to GL_TRUE)
	GLuint world = glGetUniformLocation( simpleShader->getProgram(), "world" );
	glUniformMatrix4fv( world, 1, GL_FALSE, (float *)(value_ptr(worldMat.transpose())) );
	GLuint view = glGetUniformLocation( simpleShader->getProgram(), "view" );
	glUniformMatrix4fv( view, 1, GL_FALSE, (float *)(value_ptr(cam->GetViewMatrix().transpose())) );
	GLuint proj = glGetUniformLocation( simpleShader->getProgram(), "proj" );
	glUniformMatrix4fv( proj, 1, GL_FALSE, (float *)(value_ptr(cam->GetProjectionMatrix().transpose())) );

	//Draw Ground
	grid.draw();
	cube.draw();

	//Draw Skinned Meshes
	glUseProgram(skinningShader->getProgram());

	vector<Matrix> Transforms;
	              
	float RunningTime = (float)((double)GetCurrentTimeMillis() - (double)m_startTime) / 1000.0f;

	skinningShader->SetEyeWorldPos(cam->GetPosition());   
	skinningShader->SetProjMatrix(cam->GetProjectionMatrix().transpose());  
	skinningShader->SetViewMatrix(cam->GetViewMatrix().transpose());

	//Draw The Jumping BVH Model
	skinningShader->SetWorldMatrix((*BVHModel.GetModelMatrix()).transpose());  
	BVHModel.BoneTransform(RunningTime, Transforms);        
	for (uint i = 0 ; i < Transforms.size() ; i++) {
		skinningShader->SetBoneTransform(i, Transforms[i].transpose());
	}
	BVHModel.Render();

	//Draw The Guard Model
	skinningShader->SetWorldMatrix((*md5Model.GetModelMatrix()).transpose());  
	md5Model.BoneTransform(RunningTime, Transforms);        
	for (uint i = 0 ; i < Transforms.size() ; i++) {
		skinningShader->SetBoneTransform(i, Transforms[i].transpose());
	}
	md5Model.Render();

	// Bind default framebuffer
	rendertarget.unbind();
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	quad.draw(rendertarget.getTexture());
}

void Scene::UpdateViewport(int width, int height) {
	cam->ComputeOrthographicMatrix(width, height);
	rendertarget.init(width, height);
}

long long Scene::GetCurrentTimeMillis(){
	return GetTickCount();
}

float Scene::time() {
    static __int64 start = 0;
    static __int64 frequency = 0;

    if (start==0) {
        QueryPerformanceCounter((LARGE_INTEGER*)&start);
        QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
        return 0.0f;
    }

    __int64 counter = 0;
    QueryPerformanceCounter((LARGE_INTEGER*)&counter);
    return (float) ((counter - start) / double(frequency));
}