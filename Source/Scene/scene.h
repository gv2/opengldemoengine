/****************************************************************************************
*****************************************************************************************

  Creation Date: 01/06/2013

  Rowan Hughes - Scene Container and Interface Class

*****************************************************************************************
****************************************************************************************/

#ifndef __SCENE_H__
#define	__SCENE_H__

#include <assert.h>
#include <map>
#include "include.h"
#include "GLInterface/shader.h"
#include "GLInterface/skinning_shader.h"
#include "GLInterface/depth_shader.h"
#include "GLInterface/fullscreen_quad.h"
#include "Rendering/skybox.h"
#include "Camera/camera.h"
#include "Geometry/cube.h"
#include "Geometry/grid.h"
#include "Geometry/quad.h"
#include "Geometry/mesh.h"
#include "GLInterface/framebuffer.h"

const float dt = 0.001f; 
float time();

class Scene {

public:
	Scene();
	~Scene();

	void Init(int w_width, int w_height);
	void Update();
	void Render();
	void UpdateViewport(int width, int height);

	Camera*     cam;

private:
	//Shader Variables
	Shader*     simpleShader;
	SkinningShader* skinningShader;
	DepthShader* depthShader;
	Matrix   worldMat;

	//World Objects - Simple Shape Types (Plane, Box, Etc.)
	Grid        grid;
	Cube        cube;

	//Time Variables
	long long m_startTime;

	//Render Target
	FrameBuffer rendertarget;
	DepthBuffer depthtarget;
	
	//Quad To render our rendertarget
	Quad quad;

	//Skybox
	SkyBox skybox;

	//Mesh Varialbes
	DirectionalLight directionalLight;
	Mesh BVHModel; 
	Mesh md5Model; 

	long long GetCurrentTimeMillis();
	float time();
	float t;
	float currentTime;
	float accumulator;
};

#endif