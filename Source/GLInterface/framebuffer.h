#define __SHADER_H__

#include "include.h"

#define INVALID_UNIFORM_LOCATION 0xFFFFFFFF

class FrameBuffer {

public:
	FrameBuffer(){
	}

	~FrameBuffer(){
	   //Delete resources
	   glDeleteTextures(1, &fb_tex);
	   glDeleteRenderbuffersEXT(1, &rb);
	   //Bind 0, which means render to back buffer, as a result, fb is unbound
	   glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
	   glDeleteFramebuffersEXT(1, &fb);
	}

	void init(int width, int height) {
		glGenTextures(1,&fb_tex);
		// "Bind" the newly created texture : all future texture functions will modify this texture
		glBindTexture(GL_TEXTURE_2D, fb_tex);
		// Give an empty image to OpenGL ( the last "0" )
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); // automatic mipmap
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGBA8,
			width,
			height,
			0,
			GL_BGRA,
			GL_UNSIGNED_BYTE,
			0);		
		glBindTexture(GL_TEXTURE_2D, 0);
		glGenerateMipmap(GL_TEXTURE_2D);

		glGenRenderbuffers(1, &rb);
		glBindRenderbuffer(GL_RENDERBUFFER, rb);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);	
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		
		glGenFramebuffers(1, &fb);
		glBindFramebuffer(GL_FRAMEBUFFER, fb);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fb_tex, 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb);
		
		GLenum draw_bufs[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers (1, draw_bufs);

		checkFBOStatus();
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	GLuint getTexture() {
		return fb_tex;
	}

	void recalculate(int width, int height) {
		glBindTexture(GL_TEXTURE_2D, fb_tex);

		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			width,
			height,
			0,
			GL_RGB,
			GL_UNSIGNED_BYTE,
			0);		
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindRenderbuffer(GL_RENDERBUFFER, rb);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);	
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}

	void bind() {
		glBindFramebuffer(GL_FRAMEBUFFER, fb);
		glBindRenderbuffer(GL_RENDERBUFFER, fb);
	}
	 
	void unbind() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void checkFBOStatus() {
		GLenum status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
 
		const char *err_str = 0;
		char buf[80];
 
		if( status != GL_FRAMEBUFFER_COMPLETE ) {
		switch( status ) {
			case GL_FRAMEBUFFER_UNSUPPORTED:
			err_str = "UNSUPPORTED";
			break;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			err_str = "INCOMPLETE ATTACHMENT";
			break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			err_str = "INCOMPLETE DRAW BUFFER";
			break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			err_str = "INCOMPLETE READ BUFFER";
			break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			err_str = "INCOMPLETE MISSING ATTACHMENT";
			break;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			err_str = "INCOMPLETE MULTISAMPLE";
			break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
			err_str = "INCOMPLETE LAYER TARGETS";
			break;
 
			default:
			sprintf( buf, "0x%x", status );
			err_str = buf ;
			break;
		}
 
		printf( "ERROR: glCheckFramebufferStatus() returned '%s'", err_str );
	}
}

protected:	
	GLuint fb;
	GLuint rb;
	GLuint fb_tex;
	int width, height;
};



class DepthBuffer : public FrameBuffer {

public:
	DepthBuffer(){
	}

	~DepthBuffer(){
	   //Delete resources
	   glDeleteTextures(1, &fb_tex);
	   glDeleteRenderbuffersEXT(1, &rb);
	   //Bind 0, which means render to back buffer, as a result, fb is unbound
	   glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
	   glDeleteFramebuffersEXT(1, &fb);
	}

	void init(int width, int height) {
		glGenTextures(1,&fb_tex);
		// "Bind" the newly created texture : all future texture functions will modify this texture
		glBindTexture(GL_TEXTURE_2D, fb_tex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glTexImage2D(GL_TEXTURE_2D, 
			0, 
			GL_DEPTH_COMPONENT32, 
			width, 
			height, 
			0,
            GL_DEPTH_COMPONENT, 
			GL_FLOAT, 
			0);
	
		glBindTexture(GL_TEXTURE_2D, 0);
		
		glGenRenderbuffers(1, &rb);
		glBindRenderbuffer(GL_RENDERBUFFER, rb);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);	
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		
		glGenFramebuffers(1, &fb);
		glBindFramebuffer(GL_FRAMEBUFFER, fb);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, fb_tex, 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb);

		GLenum draw_bufs[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers (1, draw_bufs);

		checkFBOStatus();
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
};