/****************************************************************************************
*****************************************************************************************

  Creation Date: 17/12/2012

  Rowan Hughes - Depth Shader Interface

*****************************************************************************************
****************************************************************************************/

#ifndef __DEPTH_SHADER_H__
#define __DEPTH_SHADER_H__

#include "shader.h"

class DepthShader : public Shader {

public:
    DepthShader();

    virtual bool Init();

    void SetWorldMatrix(const Matrix& World);
    void SetProjMatrix(const Matrix& Proj);
    void SetViewMatrix(const Matrix& View);
	void SetFarClippingPlane(float far);

protected:  
	GLuint m_WorldMatrixLocation;
	GLuint m_ProjMatrixLocation;
	GLuint m_ViewMatrixLocation;
	GLuint m_FarClipLocation;
};

#endif