#include <limits.h>
#include <string>
#include <assert.h>
#include "skybox_shader.h"

using namespace std;


SkyboxShader::SkyboxShader() : Shader("./Shaders/skyboxVP.txt","./Shaders/skyboxFP.txt") {   
}

bool SkyboxShader::Init() { 
    m_WorldMatrixLocation = GetUniformLocation("gWorld");
    m_ProjMatrixLocation = GetUniformLocation("gProj");
    m_ViewMatrixLocation = GetUniformLocation("gView");    
    m_textureLocation = GetUniformLocation("gCubemapTexture");

    return true;
}

void SkyboxShader::SetWorldMatrix(const Matrix& World) {
    glUniformMatrix4fv(m_WorldMatrixLocation, 1, GL_FALSE, value_ptr(World));
}

void SkyboxShader::SetProjMatrix(const Matrix& Proj) {
    glUniformMatrix4fv(m_ProjMatrixLocation, 1, GL_FALSE,value_ptr(Proj));
}

void SkyboxShader::SetViewMatrix(const Matrix& View) {
    glUniformMatrix4fv(m_ViewMatrixLocation, 1, GL_FALSE, value_ptr(View));
}

void SkyboxShader::SetTextureUnit(uint TextureUnit) {
    glUniform1i(m_textureLocation, TextureUnit);
}