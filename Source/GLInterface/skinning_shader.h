/****************************************************************************************
*****************************************************************************************

  Creation Date: 17/12/2012

  Rowan Hughes - Skinning Shader Interface

*****************************************************************************************
****************************************************************************************/

#ifndef __SKINNING_SHADER_H__
#define __SKINNING_SHADER_H__

#include "shader.h"

struct BaseLight {
    Vector3 Color;
    float AmbientIntensity;
    float DiffuseIntensity;

    BaseLight() {
        Color = Vector3(0.0f, 0.0f, 0.0f);
        AmbientIntensity = 0.0f;
        DiffuseIntensity = 0.0f;
    }
};

struct DirectionalLight : public BaseLight {        
    Vector3 Direction;

    DirectionalLight() {
        Direction = Vector3(0.0f, 0.0f, 0.0f);
    }
};

struct PointLight : public BaseLight {
    Vector3 Position;

    struct {
        float Constant;
        float Linear;
        float Exp;
    } Attenuation;

    PointLight() {
        Position = Vector3(0.0f, 0.0f, 0.0f);
        Attenuation.Constant = 1.0f;
        Attenuation.Linear = 0.0f;
        Attenuation.Exp = 0.0f;
    }
};

struct SpotLight : public PointLight {
    Vector3 Direction;
    float Cutoff;

    SpotLight() {
        Direction = Vector3(0.0f, 0.0f, 0.0f);
        Cutoff = 0.0f;
    }
};

class SkinningShader : public Shader {

public:
    static const uint MAX_POINT_LIGHTS = 2;
    static const uint MAX_SPOT_LIGHTS = 2;
    static const uint MAX_BONES = 100;

    SkinningShader();

    virtual bool Init();

    void SetWorldMatrix(const Matrix& World);
    void SetProjMatrix(const Matrix& Proj);
    void SetViewMatrix(const Matrix& View);
    void SetColorTextureUnit(uint TextureUnit);
    void SetDirectionalLight(const DirectionalLight& Light);
    void SetPointLights(uint NumLights, const PointLight* pLights);
    void SetSpotLights(uint NumLights, const SpotLight* pLights);
    void SetEyeWorldPos(const Vector3& EyeWorldPos);
    void SetMatSpecularIntensity(float Intensity);
    void SetMatSpecularPower(float Power);
    void SetBoneTransform(uint Index, const Matrix& Transform);

protected:  
	GLuint m_WorldMatrixLocation;
	GLuint m_ProjMatrixLocation;
	GLuint m_ViewMatrixLocation;
	GLuint m_colorTextureLocation;
	GLuint m_eyeWorldPosLocation;
	GLuint m_matSpecularIntensityLocation;
	GLuint m_matSpecularPowerLocation;
	GLuint m_numPointLightsLocation;
	GLuint m_numSpotLightsLocation;

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Direction;
	} m_dirLightLocation;

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Position;
		struct {
			GLuint Constant;
			GLuint Linear;
			GLuint Exp;
		} Atten;
	} m_pointLightsLocation[MAX_POINT_LIGHTS];

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Position;
		GLuint Direction;
		GLuint Cutoff;
		struct {
			GLuint Constant;
			GLuint Linear;
			GLuint Exp;
		} Atten;
	} m_spotLightsLocation[MAX_SPOT_LIGHTS];
    
	GLuint m_boneLocation[MAX_BONES];
};


#endif