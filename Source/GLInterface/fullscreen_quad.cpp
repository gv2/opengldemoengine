#include <limits.h>
#include <string>
#include <assert.h>
#include "fullscreen_quad.h"

using namespace std;


QuadShader::QuadShader() : Shader("./Shaders/quadShaderVP.txt","./Shaders/quadShaderFP.txt") {   
}

bool QuadShader::Init() { 
    	m_textureLocation = GetUniformLocation("tex");

    return true;
}
