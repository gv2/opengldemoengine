#ifndef __SHADER_H__
#define __SHADER_H__

#include "include.h"

#define INVALID_UNIFORM_LOCATION 0xFFFFFFFF

class Shader {

public:
	Shader(char* vs, char* ps);
	~Shader(void){};

	GLuint getProgram(){return program;};

protected:
	const char* vSF;
	const char* fSF;
	GLuint program;

	//  Helper function to load vertex and fragment shader files
	GLuint LoadShader(const char* vShaderFile, const char* fShaderFile);

	//  Function that parses Shader files
	char* readShaderSource(const char* shaderFile);  

	GLuint GetUniformLocation(const char* pUniformName);

	GLuint GetProgramParam(GLuint param);
};

#endif // __SHADER_H__