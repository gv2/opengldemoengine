/****************************************************************************************
*****************************************************************************************

  Creation Date: 17/12/2012

  Rowan Hughes - Simple Skybox Shader

*****************************************************************************************
****************************************************************************************/

#ifndef __SKYBOX_SHADER_H__
#define __SKYBOX_SHADER_H__

#include "shader.h"

class SkyboxShader : public Shader {

public:
    SkyboxShader();

    virtual bool Init();

    void SetWorldMatrix(const Matrix& World);
    void SetProjMatrix(const Matrix& Proj);
    void SetViewMatrix(const Matrix& View);
    void SetTextureUnit(uint TextureUnit);

protected:  
	GLuint m_WorldMatrixLocation;
	GLuint m_ProjMatrixLocation;
	GLuint m_ViewMatrixLocation;
	GLuint m_textureLocation;
};


#endif