#include <limits.h>
#include <string>
#include <assert.h>
#include "depth_shader.h"

using namespace std;


DepthShader::DepthShader() : Shader("./Shaders/depthShaderVP.txt","./Shaders/depthShaderFP.txt") {   
}

bool DepthShader::Init() { 
    m_WorldMatrixLocation = GetUniformLocation("gWorld");
    m_ProjMatrixLocation = GetUniformLocation("gProj");
    m_ViewMatrixLocation = GetUniformLocation("gView");    
	m_FarClipLocation = GetUniformLocation("gFar");   

    return true;
}

void DepthShader::SetWorldMatrix(const Matrix& World) {
	const float* temp = value_ptr(World);
    glUniformMatrix4fv(m_WorldMatrixLocation, 1, GL_FALSE, value_ptr(World));
}

void DepthShader::SetProjMatrix(const Matrix& Proj) {
    glUniformMatrix4fv(m_ProjMatrixLocation, 1, GL_FALSE,value_ptr(Proj));
}

void DepthShader::SetViewMatrix(const Matrix& View) {
    glUniformMatrix4fv(m_ViewMatrixLocation, 1, GL_FALSE, value_ptr(View));
}

void DepthShader::SetFarClippingPlane(float far_clip) {
     glUniform1f(m_FarClipLocation, far_clip);
}