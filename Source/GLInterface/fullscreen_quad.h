/****************************************************************************************
*****************************************************************************************

  Creation Date: 17/12/2012

  Rowan Hughes - Fullscreen Quad Shader

*****************************************************************************************
****************************************************************************************/

#ifndef __QUAD_SHADER_H__
#define __QUAD_SHADER_H__

#include "shader.h"

class QuadShader : public Shader {

public:
    QuadShader();
	bool Init();

	void SetTextureUnit(uint TextureUnit) {
		glUniform1i(m_textureLocation, TextureUnit);
	}

private:
	GLuint m_textureLocation;
};


#endif //__QUAD_SHADER_H__