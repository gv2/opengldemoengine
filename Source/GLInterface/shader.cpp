#include "shader.h"

// Create a GLSL program object from vertex and fragment shader files
Shader::Shader(char* vs, char* ps) {
	vSF = vs;
	fSF = ps;
   
	program = LoadShader(vSF,fSF);
}

//Returns a pointer to your shader
GLuint Shader::LoadShader(const char* vShaderFile, const char* fShaderFile) {
	struct CurrShader {
		const char*  filename;
		GLenum       type;
		GLchar*      source;
	}  
    
    shaders[2] = {
	    { vShaderFile, GL_VERTEX_SHADER, NULL },
	    { fShaderFile, GL_FRAGMENT_SHADER, NULL }
    };

    GLuint program = glCreateProgram();
    
    for ( int i = 0; i < 2; ++i )  {
		CurrShader& s = shaders[i];
	    s.source = readShaderSource( s.filename );
	    
		if ( shaders[i].source == NULL ) {
			std::cerr << "Failed to read " << s.filename << std::endl;
			exit( EXIT_FAILURE );
		}

		GLuint shader = glCreateShader( s.type );
		glShaderSource( shader, 1, (const GLchar**) &s.source, NULL );
		glCompileShader( shader );

		GLint  compiled;
		glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
	    
		if ( !compiled ) {
			std::cerr << s.filename << " failed to compile:" << std::endl;
			GLint  logSize;
			glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
			char* logMsg = new char[logSize];
			glGetShaderInfoLog( shader, logSize, NULL, logMsg );
			std::cerr << logMsg << std::endl;
			delete [] logMsg;

			exit( EXIT_FAILURE );
		}

		delete [] s.source;

		glAttachShader( program, shader );
	}

    //link  and error check
    glLinkProgram(program);

    GLint  linked;
    glGetProgramiv( program, GL_LINK_STATUS, &linked );
    
    if ( !linked ) {
	    std::cerr << "Shader program failed to link" << std::endl;
	    GLint  logSize;
	    glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logSize);
	    char* logMsg = new char[logSize];
	    glGetProgramInfoLog( program, logSize, NULL, logMsg );
	    std::cerr << logMsg << std::endl;
	    delete [] logMsg;

	    exit( EXIT_FAILURE );
    }

    return program;
}

// Create a NULL-terminated string by reading the provided file
char* Shader::readShaderSource(const char* shaderFile) {   
    FILE* fp = fopen(shaderFile, "rb"); //!->Why does binary flag "RB" work and not "R"... wierd msvc thing?

    if ( fp == NULL ) { return NULL; }

    fseek(fp, 0L, SEEK_END);
    long size = ftell(fp);

    fseek(fp, 0L, SEEK_SET);
    char* buf = new char[size + 1];
    fread(buf, 1, size, fp);
    buf[size] = '\0';

    fclose(fp);

    return buf;
}

//Returns a pointer to a shader constant
GLuint Shader::GetUniformLocation(const char* pUniformName) {
	GLuint location = glGetUniformLocation(program, pUniformName);

    if (location == INVALID_OGL_VALUE) {
        fprintf(stderr, "Warning! Unable to get the location of uniform '%s'\n", pUniformName);
    }

    return location;
}

GLuint Shader::GetProgramParam(GLuint param) {
    GLint ret;
    glGetProgramiv(program, param, &ret);
    return ret;
}