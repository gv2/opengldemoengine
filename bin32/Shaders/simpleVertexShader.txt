#version 330

in vec4 vPosition;
in vec4 vColor;
out vec4 color;

uniform mat4 world;
uniform mat4 view;
uniform mat4 proj;

void main(){
	color = vColor;
	gl_Position = proj * view * world * vPosition;
}